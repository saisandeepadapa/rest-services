package com.rest.services.restservices.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.services.restservices.model.User;
import com.rest.services.restservices.service.UserService;

@RestController
@RequestMapping("/user")
public class SampleController {
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/add-user")
	public String addUser(@RequestBody User user ) {
		userService.addUser(user);
		return "data created";
		
	}
	                                               
	@GetMapping(value="/get-user/{id}", produces = {"application/xml","application/json"})
	public ResponseEntity<User> getUser(@PathVariable("id") int id) {
		return userService.getUserById(id);
	}
	
	@PostMapping("/delete/{id}")
	public ResponseEntity<Object> deleteById(@PathVariable("id") int id) {	
		return userService.deleteById(id);
	}
	
	@GetMapping(value="/get-all-users")
	public List<User> getAllUsers(){
		return userService.getAllUsers();
	}
	

}

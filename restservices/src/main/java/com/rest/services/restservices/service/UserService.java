package com.rest.services.restservices.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.rest.services.restservices.entity.UserEntity;
import com.rest.services.restservices.model.User;
import com.rest.services.restservices.repository.UserRepository;
import com.rest.services.restservices.runtimeexception.UserNotFound;

@Service
public class UserService {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private UserRepository userRepository;

	static List<User> userList = new ArrayList<>();



	public ResponseEntity<User> getUserById(int id) {
		if(!userRepository.findById(id).isPresent() ){
			throw new UserNotFound("user not found");
		}else {
			return new ResponseEntity<>(  modelMapper.map(userRepository.findById(id).get(),User.class),HttpStatus.OK);
		}

	}


	public void addUser(User user) {

		UserEntity userEntity = modelMapper.map(user,UserEntity.class);

		userRepository.save(userEntity);
		userList.add(user);

	}


	public List<User> getAllUsers() {
		System.out.println(userRepository.findAll());
		List<UserEntity> userEntitys = userRepository.findAll();
		List<User> allUserData = new ArrayList<>();
		allUserData = userEntitys.stream().map(userdata->modelMapper.map(userdata,User.class)).collect(Collectors.toList());
		return allUserData;
	}


	public ResponseEntity<Object> deleteById(int id) {
		if(!userRepository.findById(id).isPresent() ){
			throw new UserNotFound("user not found to delete");
		}else {
			userRepository.deleteById(id);
			return new ResponseEntity<Object>("data deleted",HttpStatus.OK);
		}
	}




}

package com.rest.services.restservices.runtimeexception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UserEceptionControllerAdvice  {
	
	@ExceptionHandler(value= UserNotFound.class)
	public ResponseEntity<ExceptionResponseClass> userNotFoundException(UserNotFound exception){
		ExceptionResponseClass exceptionResponseClass = new ExceptionResponseClass();
		exceptionResponseClass.setMessage(exception.getMessage());
		//exceptionResponseClass.setStatus(HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(exceptionResponseClass,HttpStatus.NOT_FOUND);
		
	}

}

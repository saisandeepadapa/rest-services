package com.rest.services.restservices.model;

import lombok.Data;

@Data
public class User {
	
	private String userName;
	private String firstName;
	private String lastName;
	private int  id;
	

}
